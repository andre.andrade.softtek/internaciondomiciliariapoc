package com.softtek;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.googlecode.objectify.ObjectifyService;

@Api(name = "paciente",
        description = "Api relacionada a los pacientes",
        version = "v1")
public class PacienteAPI {
    @ApiMethod(httpMethod = ApiMethod.HttpMethod.POST,
            path = "add")
    public Paciente addPaciente(Paciente paciente){

        ObjectifyService.ofy().save().entity(paciente).now();

        return paciente;
    }
}
