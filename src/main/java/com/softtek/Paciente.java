package com.softtek;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Paciente {
    @Id
    private Long id;
    @Index
    private Long afiliadoId;
    @Index
    private String nombre;
    @Index
    private String apellido;
    @Index
    private String dni;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAfiliadoId() {
        return afiliadoId;
    }

    public void setAfiliadoId(Long afiliadoId) {
        this.afiliadoId = afiliadoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
}
