package com.softtek;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.googlecode.objectify.ObjectifyService;

/**
  * Add your first API methods in this class, or you may create another class. In that case, please
  * update your web.xml accordingly.
 **/
@Api(name = "caso",
     description = "Api relacionada a los casos",
     version = "v1")
public class CasoAPI {
    @ApiMethod(httpMethod = ApiMethod.HttpMethod.GET,
            path = "asignarCaso")
    public Caso asignarCasoByAuditor( @Named("idAuditor") long id){
        return ObjectifyService.ofy().load().type(Caso.class).id(id).now();
    }

    @ApiMethod(httpMethod = ApiMethod.HttpMethod.POST,
            path = "add")
    public Caso addCaso(Caso caso){

        ObjectifyService.ofy().save().entity(caso).now();

        return caso;
    }
}
