package com.softtek;

import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class ObjectifyStartup extends HttpServlet {

    @Override
    public void init() throws ServletException {
        ObjectifyService.register(Caso.class);
        ObjectifyService.register(Paciente.class);
    }
}